package fr.eni.enichucknorris.utils;

import java.lang.reflect.Array;
import java.util.ArrayList;

import fr.eni.enichucknorris.model.Joke;

public class CustomResponse {
    private String type;
    private Joke value;

    public CustomResponse(String type, Joke value) {
        this.type = type;
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Joke getValue() {
        return value;
    }

    public void setValue(Joke value) {
        this.value = value;
    }
}

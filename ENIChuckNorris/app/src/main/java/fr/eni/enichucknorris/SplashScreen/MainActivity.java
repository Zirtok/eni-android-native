package fr.eni.enichucknorris.SplashScreen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;

import java.util.Timer;
import java.util.TimerTask;


import fr.eni.enichucknorris.AppActivity;
import fr.eni.enichucknorris.R;
import fr.eni.enichucknorris.listCategory.ListByCategoryActivity;

public class MainActivity extends AppActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                Intent intentHome = new Intent(MainActivity.this, ListByCategoryActivity.class);
                startActivity(intentHome);
                // ou finish();
            }
        }, 3000);

    }



}

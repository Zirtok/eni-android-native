package fr.eni.enichucknorris;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import fr.eni.enichucknorris.customname.CustomNameActivity;
import fr.eni.enichucknorris.listCategory.ListByCategoryActivity;

public class AppActivity extends AppCompatActivity {

    //Affichage du menu principal
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_principal, menu);
        return super.onCreateOptionsMenu(menu);
    }

    //Redirection sur les item de l'ActionBar
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuMyJoke:
                Intent intent = new Intent(this, CustomNameActivity.class);
                startActivity(intent);
                break;
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    //Affichage de la flèche de retour
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getSupportActionBar() != null) {
            if(!(this instanceof ListByCategoryActivity)) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            }
        }
    }

}

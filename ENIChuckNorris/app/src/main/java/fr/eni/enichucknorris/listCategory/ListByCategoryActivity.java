package fr.eni.enichucknorris.listCategory;

import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.Dialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import fr.eni.enichucknorris.AppActivity;
import fr.eni.enichucknorris.R;
import fr.eni.enichucknorris.SplashScreen.MainActivity;
import fr.eni.enichucknorris.model.Joke;
import fr.eni.enichucknorris.utils.Api;

public class ListByCategoryActivity extends AppActivity {
    private ListView listViewJokes;
    private Button btnCategoryExplicit;
    private Button btnCategoryNerdy;
    private Button btnAllCat;
    private EditText editViewNbJokes;
    private ArrayAdapter<Joke> adapter;
    private List<Joke> listJokes = new ArrayList<>();

    private SwipeRefreshLayout swipeContainer;
    private String type;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_by_category);

        // récupération des champs
        editViewNbJokes = findViewById(R.id.editViewNbJokes);
        listViewJokes = findViewById(R.id.listViewData);
        btnCategoryExplicit = findViewById(R.id.buttonCatExplicit);
        btnCategoryNerdy = findViewById(R.id.buttonCatNerdy);
        btnAllCat = findViewById(R.id.buttonAllCat);
        swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);

        // Configuration du pull to refresh
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(type == "explicit"){
                    btnCategoryExplicit.performClick();
                }else if(type == "nerdy"){
                    btnCategoryNerdy.performClick();
                }else{
                    btnAllCat.performClick();
                }

            }
        });
        // Couleurs pull to refresh
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_dark,
                android.R.color.holo_blue_bright,
                android.R.color.holo_blue_light);

        // listeners buttons
        btnCategoryExplicit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                getJokes("explicit");
                type = "explicit";
            }
        });

        btnCategoryNerdy.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                getJokes("nerdy");
                type = "nerdy";
            }
        });

        btnAllCat.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                getJokes("");
                type = "";
            }
        });

        // custom adapter
        adapter = new JokeAdapter(
                ListByCategoryActivity.this,
                R.layout.item_joke,
                listJokes
        );

        listViewJokes.setAdapter(adapter);
    }

    private void getJokes(String category) {
        List<Joke> listJokesResult = null;
        String url;
        int nbJokes;

        // nombre de Joke à récupérer (10 par défaut)
        if(TextUtils.isEmpty(editViewNbJokes.getText().toString())) {
            nbJokes = 10;
        }else{
            nbJokes = Integer.valueOf(editViewNbJokes.getText().toString());
        }

        // filtre par catégorie ou non
        if(category.equals("")){
            url = "https://api.icndb.com/jokes/random/"+nbJokes;
        }else{
            url = "https://api.icndb.com/jokes/random/"+nbJokes+"?limitTo=["+category+"]";
        }

        // appel API
        try {
            listJokesResult = Api.getJokesByCategory(url);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // mise à jour de la liste
        if(listJokesResult != null)
        {
            listJokes.clear();
            for(Joke joke : listJokesResult){
                listJokes.add(joke);
                // Fin de tache pull to refresh
                swipeContainer.setRefreshing(false);

            }
            adapter.notifyDataSetChanged();
        }
    }
}

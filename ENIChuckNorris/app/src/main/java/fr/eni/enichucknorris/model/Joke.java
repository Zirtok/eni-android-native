package fr.eni.enichucknorris.model;

import java.util.ArrayList;

public class Joke {
    private String joke;
    private int id;
    private ArrayList<String> categories;

    public Joke(String joke, int id, ArrayList<String> categories) {
        this.joke = joke;
        this.id = id;
        this.categories = categories;
    }

    public String getJoke() {
        return joke.replaceAll("&quot;","\"");
    }

    public void setJoke(String joke) {
        this.joke = joke;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ArrayList<String> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<String> categories) {
        this.categories = categories;
    }
}

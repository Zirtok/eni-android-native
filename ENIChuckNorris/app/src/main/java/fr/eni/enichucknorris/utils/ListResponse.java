package fr.eni.enichucknorris.utils;

import java.util.List;

import fr.eni.enichucknorris.model.Joke;

public class ListResponse {
    private String type;
    private List<Joke> value;

    public ListResponse(String type, List<Joke> value) {
        this.type = type;
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Joke> getValue() {
        return value;
    }

    public void setValue(List<Joke> value) {
        this.value = value;
    }
}

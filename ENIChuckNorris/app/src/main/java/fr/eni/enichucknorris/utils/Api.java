package fr.eni.enichucknorris.utils;

import android.util.Log;

import com.google.gson.Gson;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.concurrent.ExecutionException;

import fr.eni.enichucknorris.model.Joke;

public class Api {
    public static Joke getRandomJoke(String url) throws ExecutionException, InterruptedException, UnsupportedEncodingException {
        Log.i("Custom log ici", "--------------------------------------------------------------");
        Log.i("Custom log ici", "debut methode");
        Log.i("Custom log ici", "--------------------------------------------------------------");
        String result;
        HttpGetRequest getRequest = new HttpGetRequest();   //Perform the doInBackground method, passing in our url
        result =  getRequest.execute(url).get();

        Log.i("Custom log ici", "result raw: " + result);
        Log.i("Custom log ici", "--------------------------------------------------------------");
        Gson g = new Gson();
        CustomResponse customResponse = g.fromJson(result, CustomResponse.class);
        Log.i("Custom log ici", customResponse.getValue().getJoke());
        Log.i("Custom log ici", "--------------------------------------------------------------");
        return customResponse.getValue();
    }

    public static List<Joke> getJokesByCategory(String url) throws ExecutionException, InterruptedException, UnsupportedEncodingException {
        Log.i("Custom log ici", "--------------------------------------------------------------");
        Log.i("Custom log ici", "debut methode");
        Log.i("Custom log ici", "--------------------------------------------------------------");
        String result;
        HttpGetRequest getRequest = new HttpGetRequest();   //Perform the doInBackground method, passing in our url
        result =  getRequest.execute(url).get();

        Log.i("Custom log ici", "result raw: " + result);
        Log.i("Custom log ici", "--------------------------------------------------------------");
        Gson g = new Gson();
        ListResponse listResponse = g.fromJson(result, ListResponse.class);
        Log.i("Custom log ici", listResponse.getValue().toString());
        Log.i("Custom log ici", "--------------------------------------------------------------");
        return listResponse.getValue();
    }
}

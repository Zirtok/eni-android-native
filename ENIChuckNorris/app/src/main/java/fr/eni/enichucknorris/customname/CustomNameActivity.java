package fr.eni.enichucknorris.customname;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.concurrent.ExecutionException;

import fr.eni.enichucknorris.AppActivity;
import fr.eni.enichucknorris.R;
import fr.eni.enichucknorris.model.Joke;
import fr.eni.enichucknorris.utils.Api;


public class CustomNameActivity extends AppActivity {
    private EditText firstnameText;
    private EditText lastnameText;
    private TextView jokeView;
    private Button getJokeButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_name);

        firstnameText = findViewById(R.id.firstname);
        lastnameText = findViewById(R.id.lastname);
        jokeView = findViewById(R.id.joke);
        getJokeButton = findViewById(R.id.buttonGetJoke);
        getJokeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                getNewJoke();
            }
        });
    }

    private void getNewJoke() {
        Joke j = null;
        String firstname = (String) firstnameText.getText().toString();
        String lastname = (String) lastnameText.getText().toString();
        try {
            String url = "https://api.icndb.com/jokes/random?firstName="+firstname+"&&lastName="+lastname;
            Log.i("url", url);
            j = Api.getRandomJoke(url);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(j != null)
        {
            jokeView.setText(j.getJoke());
        }
    }
}

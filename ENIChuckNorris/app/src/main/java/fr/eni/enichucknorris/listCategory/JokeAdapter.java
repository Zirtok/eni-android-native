package fr.eni.enichucknorris.listCategory;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

import fr.eni.enichucknorris.R;
import fr.eni.enichucknorris.model.Joke;

public class JokeAdapter  extends ArrayAdapter<Joke> {
    private int resId;

    public JokeAdapter(@NonNull Context context, int resource, @NonNull List<Joke> objects) {
        super(context, resource, objects);

        resId = resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder myViewHolder;

        if(convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(resId, null);

            myViewHolder = new ViewHolder();

            myViewHolder.textViewJoke = convertView.findViewById(R.id.textViewJoke);

            // enregistrement du ViewHolder
            convertView.setTag(myViewHolder);
        } else {
            myViewHolder = (ViewHolder) convertView.getTag();
        }

        // Récupération de la Joke en cours
        Joke joke = getItem(position);

        // Affichage de la view
        myViewHolder.textViewJoke.setText(joke.getJoke());

        return convertView;
    }

    class ViewHolder {
        TextView textViewJoke;
    }
}
